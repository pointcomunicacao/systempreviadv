<!doctype html>
<html lang="pt-br">

<head>
    <!-- Meta tags obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>PREVI ADV - Assessoria Previdenciária</title>

    <!-- SEO -->
    <?php include ( '_inc/metatag.php' );?>

    <!-- CSS, JS e complementos -->
    <?php include ( '_inc/head.php' );?>

</head>

<body class="d-flex flex-column">

<div id="page-content">

    <!-- start:: header -->
    <header class="fixed-top">
        <div class="container">
            <div class="row">
                
                <!-- start:: menu -->
                <?php include ( '_inc/menu.php' ); ?>
                <!-- end:: menu -->

            </div>
        </div>
    </header>
    <!-- end:: header -->