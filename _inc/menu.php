<div class="col-12">

   
    <nav class="navbar navbar-expand-lg navbar-light">
        
        <!-- start:: logotipo -->
        <a href="index.php" class="navbar-brand pointcom-logotipo">
            <img src="assets/img/logotipo.png" alt="" loading="lazy">
        </a>
        <!-- end:: logotipo -->

        <button 
            class="menu navbar-toggler" 
            type="button" 
            data-toggle="collapse" 
            data-target="#navbarNavDropdown" 
            aria-controls="navbarNavDropdown" 
            aria-expanded="false" 
            aria-label="Toggle navigation"
            onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))" 
        >
            <svg width="60" height="60" viewBox="0 0 100 100">
                <path class="line line1" d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                <path class="line line2" d="M 20,50 H 80" />
                <path class="line line3" d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
            </svg>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
            <ul class="navbar-nav pointcom-menu">
                <li class="nav-item dropdown">
                    <a  class="btn link-menu dropdown-toggle" 
                        href="#" id="menuDropDown" 
                        role="button" data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false">
                            Produto
                    </a>

                    <div class="dropdown-menu" aria-labelledby="menuDropDown">
                        <a class="dropdown-item" href="page-blank.php">Submenu 1</a>
                        <a class="dropdown-item" href="page-blank.php">Submenu 2</a>
                        <a class="dropdown-item" href="page-blank.php">Submenu 3</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="btn link-menu" href="#precos">Preço</a>
                </li>
                
                <li class="nav-item">
                    <a class="btn link-menu" href="peticoes.php">Petições</a>
                </li>

                <li class="nav-item">
                    <a href="" class="btn link-menu">Login</a>
                </li>
                
                <li class="nav-item">
                    <a href="assinar.php" class="btn btn-info link-menu">Assine Agora</a>
                </li>
            </ul>
        </div>
    </nav>
    

</div>