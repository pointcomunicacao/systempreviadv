<!-- material icons -->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp"/>

<!-- bootstrap -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">

<!-- custom css -->
<link rel="stylesheet" href="assets/css/pointcom.css">

<!-- favicon -->
<link rel="icon" href="assets/img/favicon.png" />