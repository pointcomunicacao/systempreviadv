jQuery(document).ready(function ($) {

    
    /*---- RETORNAR AO TOPO ----*/
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 50) {    
            $('#return-to-top').fadeIn(300);
        } else {
            $('#return-to-top').fadeOut(300);
        }
    });

    $('#return-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });


    /*---- Class Animate Scroll ----*/
    const debounce = function (func, wait, immediate) {
        let timeout;
        return function (...args) {
            const context = this;
            const later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            const callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    const target = document.querySelectorAll('[animar-box]');
    const animationClass = 'animate';

    function animeScroll() {
        const windowTop = window.pageYOffset + ((window.innerHeight * 3) / 4);
        target.forEach(function(element){
            if((windowTop) > element.offsetTop) {
                element.classList.add(animationClass);
            } else {
                element.classList.remove(animationClass);
            }
        })
    }

    animeScroll();

    if(target.length){      
        window.addEventListener('scroll' , debounce(function(){
            animeScroll();
        }, 100));
    }

    /*---- Adicionando class no header ----*/
    $(window).scroll(function() {
        var scrollPos = $(window).scrollTop();
      
        if (scrollPos > 40) {
          $('header').addClass('fix-header');
        } else {
          $('header').removeClass('fix-header');
        }      
      });

      //SCROOL DAS SECTIONS
    $("a").on('click', function(event) {
        if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function(){
            window.location.hash = hash;
        });
        } // End if
    });
        
});
