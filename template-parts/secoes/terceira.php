<section class="secoes terceira left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Aqui vai um título para o vídeo</h1>
                <p>Cupidatat in do laboris elit nostrud. Aute magna pariatur eu aute nisi et est cupidatat aliqua in ut. Tempor sunt cupidatat non pariatur voluptate culpa in ut occaecat ut ullamco consequat. Anim proident enim reprehenderit enim in qui elit adipisicing. </p>
            </div>

            <div class="col-sm-12">
                <a class="square-image d-flex justify-content-center align-items-center"
                    data-toggle="modal" 
                    data-target="#modalvideo">
                    <span class="material-icons-outlined">play_arrow</span>
                    <img src="assets/img/news-3.jpg" class="img-fluid" alt="Fit Image">
                </a>

                <div class="modal fade" id="modalvideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="https://www.youtube.com/embed/gEPmA3USJdI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</section>