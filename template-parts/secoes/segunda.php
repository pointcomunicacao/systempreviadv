<section class="secoes segunda right">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Ut exercitation ullamco sint sint officia dolor aliquip</h1>
                <p>Cupidatat in do laboris elit nostrud. Aute magna pariatur eu aute nisi et est cupidatat aliqua in ut. Tempor sunt cupidatat non pariatur voluptate culpa in ut occaecat ut ullamco consequat. Anim proident enim reprehenderit enim in qui elit adipisicing. </p>
            </div>

            <div class="col-sm-4">
                <div class="box-content-home">
                    <span class="material-icons-outlined">mode_comment</span>
                    <h3>Eiusmod officia laborum sint</h3>
                    <p>Sit consectetur anim exercitation duis duis minim sunt Lorem nisi commodo.</p>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="box-content-home">
                    <span class="material-icons-outlined">access_time</span>
                    <h3>Eiusmod officia laborum sint</h3>
                    <p>Sit consectetur anim exercitation duis duis minim sunt Lorem nisi commodo.</p>
                </div>
            </div>
            
            <div class="col-sm-4">
                <div class="box-content-home">
                    <span class="material-icons-outlined">bookmarks</span>
                    <h3>Eiusmod officia laborum sint</h3>
                    <p>Sit consectetur anim exercitation duis duis minim sunt Lorem nisi commodo.</p>
                </div>
            </div>
        </div>
    </div>
</section>