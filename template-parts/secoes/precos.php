<section class="secoes quarta right" id="precos">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="price-box">
                    <h1>Preço</h1>
                    <p>Consectetur consectetur officia sunt laborum qui velit ea laborum aute occaecat quis.</p>
                </div>
            </div>
         </div>
         
         <div class="row">
            
            <div class="col-sm-4">
                <div class="box-col-price">
                    <h2>Titulo</h2>
                    <p class="currency"><small>R$</small>199,<small>90</small></p>
                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <a href="#" class="btn-padrao">
                        Comprar
                    </a>

                </div>
            </div><!-- end:: col-sm-4 -->

            <div class="col-sm-4">
                <div class="box-col-price destaque">
                    <h2>Titulo</h2>
                    <p class="currency"><small>R$</small>199,<small>90</small></p>
                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <a href="#" class="btn-padrao">
                        Comprar
                    </a>

                </div>
            </div><!-- end:: col-sm-4 -->

            <div class="col-sm-4">
                <div class="box-col-price">
                    <h2>Titulo</h2>
                    <p class="currency"><small>R$</small>199,<small>90</small></p>
                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <div class="box-info-price">
                        <h4>Subtitulo</h4>
                        <span>Pariatur culpa nisi et veniam incididunt deserunt commodo commodo id nostrud.</span>
                    </div>

                    <a href="#" class="btn-padrao">
                        Comprar
                    </a>

                </div>
            </div><!-- end:: col-sm-4 -->

         </div><!-- end:: row -->

         <div class="row">
             <div class="col-sm-12">
                 <div class="info-price-home">
                     <a href="" class="btn-padrao">
                         saiba mais sobre nossos planos
                     </a>
                 </div>
             </div>
         </div>
    </div>
</section>