<section class="secoes quinta noticias">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="titulo-nt">
                    <h1>Últimas notícias</h1>
                    <p>Incididunt veniam proident eiusmod labore ullamco.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="box-info-nt">
                    <div class="content-info-nt">
                        <img src="https://via.placeholder.com/385x250" alt="" class="img-fluid">
                        <h2>Titulo do Post</h2> <!-- coloca um truncate para não passar de uma linha -->
                        <p>Laboris veniam eu quis enim elit in do dolor ipsum aliqua.</p><!-- coloca um truncate para não passar de duas linhas -->
                        <div class="footer-nt">
                            <a href="" class="btn-padrao">
                                Leia mais
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="box-info-nt">
                    <div class="content-info-nt">
                        <img src="https://via.placeholder.com/385x250" alt="" class="img-fluid">
                        <h2>Titulo do Post</h2>
                        <p>Laboris veniam eu quis enim elit in do dolor ipsum aliqua.</p>
                        <div class="footer-nt">
                            <a href="" class="btn-padrao">
                                Leia mais
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="box-info-nt">
                    <div class="content-info-nt">
                        <img src="https://via.placeholder.com/385x250" alt="" class="img-fluid">
                        <h2>Titulo do Post</h2>
                        <p>Laboris veniam eu quis enim elit in do dolor ipsum aliqua.</p>
                        <div class="footer-nt">
                            <a href="" class="btn-padrao">
                                Leia mais
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="info-int-not">
                    <a href="" class="btn-padrao">
                        Ver mais notícias
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>