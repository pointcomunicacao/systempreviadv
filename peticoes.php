<?php 

include ( 'header.php' );?>

<section class="busca-peticoes">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="chamada-peticoes">
                    <h1>Chamada de ação para o box</h1>
                    <p>Nulla anim id aute ea consequat esse amet id.</p>
                </div>
                
                <p class="chamada-filtro">Filtre sua busca por:</p>
                
                <div class="btn-group pointcom-dropdown">
                    <a class="btn-padrao dropdown-toggle" href="#" role="button" id="filtro1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filtro 1
                    </a>

                    <div class="dropdown-menu" aria-labelledby="filtro1">
                        <a class="dropdown-item" href="#">Listagem 1</a>
                        <a class="dropdown-item" href="#">Listagem 1</a>
                        <a class="dropdown-item" href="#">Listagem 1</a>
                    </div>
                </div>

                <div class="btn-group pointcom-dropdown">
                    <a class="btn-padrao dropdown-toggle" href="#" role="button" id="filtro2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filtro 2
                    </a>

                    <div class="dropdown-menu" aria-labelledby="filtro1">
                        <a class="dropdown-item" href="#">Listagem 2</a>
                        <a class="dropdown-item" href="#">Listagem 2</a>
                        <a class="dropdown-item" href="#">Listagem 2</a>
                    </div>
                </div>

                <div class="btn-group pointcom-dropdown">
                    <a class="btn-padrao dropdown-toggle" href="#" role="button" id="filtro3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filtro 3
                    </a>

                    <div class="dropdown-menu" aria-labelledby="filtro1">
                        <a class="dropdown-item" href="#">Listagem 3</a>
                        <a class="dropdown-item" href="#">Listagem 3</a>
                        <a class="dropdown-item" href="#">Listagem 3</a>
                    </div>
                </div>

                <form action="" class="pointcom-form">
                    <input type="text" placeholder="Pesquise por benefícios, modelos, requerimentos etc. Ex: auxilio-doença">
                    <a href="" type="submit" class="btn-search">
                        <span class="material-icons-outlined">search</span> 
                    </a>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="listagem-peticoes">
    <div class="container">
        <div class="row">

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="box-listagem-peticoes">
                    <a href="">
                        <div class="ch-peticoes">
                            
                            <span>Petições iniciais</span>
                            <span>20/08/2020</span>
                        </div>

                        <h4>Nome da petição</h4>
                    </a>
                </div>
            </div>
            <!-- end:: loop -->

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="box-listagem-peticoes">
                    <a href="">
                        <div class="ch-peticoes">
                            
                            <span>Petições iniciais</span>
                            <span>20/08/2020</span>
                        </div>

                        <h4>Nome da petição</h4>
                    </a>
                </div>
            </div>
            <!-- end:: loop -->

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="box-listagem-peticoes">
                    <a href="">
                        <div class="ch-peticoes">
                            
                            <span>Petições iniciais</span>
                            <span>20/08/2020</span>
                        </div>

                        <h4>Nome da petição</h4>
                    </a>
                </div>
            </div>
            <!-- end:: loop -->

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="box-listagem-peticoes">
                    <a href="">
                        <div class="ch-peticoes">
                            
                            <span>Petições iniciais</span>
                            <span>20/08/2020</span>
                        </div>

                        <h4>Nome da petição</h4>
                    </a>
                </div>
            </div>
            <!-- end:: loop -->

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="box-listagem-peticoes">
                    <a href="">
                        <div class="ch-peticoes">
                            
                            <span>Petições iniciais</span>
                            <span>20/08/2020</span>
                        </div>

                        <h4>Nome da petição</h4>
                    </a>
                </div>
            </div>
            <!-- end:: loop -->

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="box-listagem-peticoes">
                    <a href="">
                        <div class="ch-peticoes">
                            
                            <span>Petições iniciais</span>
                            <span>20/08/2020</span>
                        </div>

                        <h4>Nome da petição</h4>
                    </a>
                </div>
            </div>
            <!-- end:: loop -->

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="box-listagem-peticoes">
                    <a href="">
                        <div class="ch-peticoes">
                            
                            <span>Petições iniciais</span>
                            <span>20/08/2020</span>
                        </div>

                        <h4>Nome da petição</h4>
                    </a>
                </div>
            </div>
            <!-- end:: loop -->
        </div>
    </div>
</section>

<?php include ( 'footer.php' );