
</div><!-- end:: page-content -->

<footer id="sticky-footer">
  <div class="container">
    <div class="row">
      
    <div class="col-sm-4">
        <div class="widget-footer">
          <h2>Advogados</h2>
          <ul>
            <li><a href="">1</a></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><a href="">4</a></li>
          </ul>
        </div>
      </div><!-- end:: col-sm-4 -->

      <div class="col-sm-4">
        <div class="widget-footer">
          <h2>Petições</h2>
          <ul>
            <li><a href="">1</a></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><a href="">4</a></li>
          </ul>
        </div>
      </div><!-- end:: col-sm-4 -->

      <div class="col-sm-4">
        <div class="widget-footer">
          <h2>Institucional</h2>
          <ul>
            <li><a href="">Teste de texto do gradient</a></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><a href="">4</a></li>
          </ul>
        </div>
      </div><!-- end:: col-sm-4 -->

    </div><!-- end:: row -->
  </div>

  <div class="copyright">
      <div class="container">
          <div class="row">
              <div class="col-sm-7">
                  @<?php echo date('Y');?> | PrevAdv Assessoria Previdenciária | Todos os direitos reservados
              </div>

              <div class="col-sm-5">
                  <ul class="copy-terms">
                      <li><a href="">Termos de Uso</a></li>
                      <li><a href="">Política de Privacidade</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</footer>

<!-- start:: voltar ao topo -->
<a href="javascript:" id="return-to-top">
  <span class="material-icons">keyboard_arrow_up</span>  
</a>
<!-- end:: voltar ao topo -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/pointcom.min.js"></script>
  </body>
</html>